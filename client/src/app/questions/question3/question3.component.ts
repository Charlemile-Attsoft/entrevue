import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-question3',
  templateUrl: './question3.component.html',
  styleUrls: ['./question3.component.css']
})
export class Question3Component implements OnInit {

  data = [{
    "id": "A8C2FF6C-FD6B-E3B0-137A-BE62CCF6B5C9",
    "nom": "Aiko",
    "prenom": "Duffy",
    "email": "nibh.vulputate@sapien.net",
    "carteCredit": "5393 1123 2381 3953",
    "montant": "24.90",
    "naissance": "2019-11-06"
  }, {
    "id": "C6237019-C625-D410-6F28-02FB3FDC4F98",
    "nom": "Pascale",
    "prenom": "Ramos",
    "email": "et@odioAliquamvulputate.ca",
    "carteCredit": "5212274885748065",
    "montant": "7.17",
    "naissance": "2018-07-19"
  }, {
    "id": "349A5300-E8E3-284F-6504-A17ED46B5832",
    "nom": "Wallace",
    "prenom": "Cooper",
    "email": "ac@Suspendisse.org",
    "carteCredit": "5164 4934 3113 4749",
    "montant": "4.71",
    "naissance": "2018-10-25"
  }, {
    "id": "722F6EE8-9B0C-D7BB-7FB9-D08AD6DAF6E4",
    "nom": "Ashton",
    "prenom": "Gamble",
    "email": "pede@accumsan.net",
    "carteCredit": "5113 2361 9391 1958",
    "montant": "28.43",
    "naissance": "2020-02-10"
  }, {
    "id": "37C773F9-1345-5C62-CC5B-9FAA022DD8AB",
    "nom": "Naomi",
    "prenom": "Davis",
    "email": "ac.fermentum@vulputateeu.ca",
    "carteCredit": "5527 6034 9313 2066",
    "montant": "84.82",
    "naissance": "2019-03-02"
  }, {
    "id": "319BEB93-F7AE-3DCD-76F6-C4DF6A6CE569",
    "nom": "Elton",
    "prenom": "Jimenez",
    "email": "magna.a.neque@lobortis.net",
    "carteCredit": "512 05806 91645 290",
    "montant": "77.55",
    "naissance": "2018-12-26"
  }, {
    "id": "AF803FE8-E091-C3E7-CBB6-C652F1661A78",
    "nom": "Michelle",
    "prenom": "Cash",
    "email": "vulputate@velit.ca",
    "carteCredit": "518291 8404697072",
    "montant": "64.88",
    "naissance": "2019-04-20"
  }, {
    "id": "D9101FBF-E036-10A3-4557-7A84142CF0D4",
    "nom": "Angelica",
    "prenom": "Tyson",
    "email": "massa@velvenenatis.org",
    "carteCredit": "5495 2708 6596 2872",
    "montant": "15.86",
    "naissance": "2020-01-11"
  }, {
    "id": "F7E8BBA3-944D-58EC-2A38-9905A5FDC935",
    "nom": "Devin",
    "prenom": "Noble",
    "email": "condimentum.Donec.at@gravidamaurisut.com",
    "carteCredit": "550632 353475 3318",
    "montant": "42.40",
    "naissance": "2018-07-24"
  }, {
    "id": "15458A94-1C4A-3BAF-A1AA-F32560B70E5A",
    "nom": "Leigh",
    "prenom": "Stout",
    "email": "venenatis.vel@vitae.com",
    "carteCredit": "5467 5292 8722 4086",
    "montant": "60.34",
    "naissance": "2019-08-14"
  }, {
    "id": "79DFE71A-15A8-0E00-2DA5-D03EA0244A41",
    "nom": "Debra",
    "prenom": "Alston",
    "email": "ut.quam@Sed.co.uk",
    "carteCredit": "528336 7336855598",
    "montant": "19.98",
    "naissance": "2019-04-25"
  }, {
    "id": "5EB384AA-A460-2504-C05A-2C0DE76E0B8B",
    "nom": "Jayme",
    "prenom": "Schmidt",
    "email": "lobortis.augue@metus.edu",
    "carteCredit": "533268 0570955580",
    "montant": "47.68",
    "naissance": "2020-04-17"
  }, {
    "id": "3B4D2853-EB4E-EEC4-D94C-217B1E6BC22A",
    "nom": "Leo",
    "prenom": "Bolton",
    "email": "condimentum.Donec.at@placerat.com",
    "carteCredit": "529 38604 96970 288",
    "montant": "6.18",
    "naissance": "2020-01-05"
  }, {
    "id": "D148D2C5-9E0D-7CEB-9D2D-FAE3DE056B67",
    "nom": "Nero",
    "prenom": "Black",
    "email": "consectetuer.ipsum.nunc@risus.net",
    "carteCredit": "556233 650771 2120",
    "montant": "35.56",
    "naissance": "2019-01-03"
  }, {
    "id": "4732E7F7-4697-4B11-0521-677C44B911C4",
    "nom": "Akeem",
    "prenom": "Chen",
    "email": "orci.luctus.et@atsemmolestie.net",
    "carteCredit": "5259599058669787",
    "montant": "21.53",
    "naissance": "2018-10-23"
  }, {
    "id": "15987613-D7DC-C377-44E6-603DCF34DF04",
    "nom": "Stacey",
    "prenom": "Warren",
    "email": "diam.Pellentesque.habitant@MaurismagnaDuis.com",
    "carteCredit": "5326002750937122",
    "montant": "86.10",
    "naissance": "2019-12-08"
  }, {
    "id": "63F1DC61-395C-D471-8F74-D2C32706EBA4",
    "nom": "Fredericka",
    "prenom": "Foreman",
    "email": "cursus.purus.Nullam@nullamagna.net",
    "carteCredit": "553811 966648 5493",
    "montant": "97.44",
    "naissance": "2019-04-04"
  }, {
    "id": "A61CD273-91B2-796E-5322-C0E605505879",
    "nom": "Levi",
    "prenom": "Glass",
    "email": "lectus.sit.amet@pharetrafeliseget.ca",
    "carteCredit": "5459386125976041",
    "montant": "75.32",
    "naissance": "2020-04-28"
  }, {
    "id": "7EEE9B7D-A7A6-765C-C43E-1FB2DB625448",
    "nom": "Megan",
    "prenom": "Bright",
    "email": "dui.Cum.sociis@dolornonummy.net",
    "carteCredit": "5564 0254 0310 4346",
    "montant": "81.94",
    "naissance": "2019-09-01"
  }, {
    "id": "CB1A642A-7CB7-700A-530F-C1DF8818C8BC",
    "nom": "Alexander",
    "prenom": "Glover",
    "email": "eros.turpis.non@eleifendnondapibus.org",
    "carteCredit": "5358 3512 5460 3131",
    "montant": "20.96",
    "naissance": "2019-01-07"
  }];

  constructor() { }

  ngOnInit() {
  }

}
