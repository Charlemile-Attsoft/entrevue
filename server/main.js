var express = require("express");

const app = express();
app.get('/',function(req, response)
{
    const data = [
        {
            nom: "Smith",
            prenom: "John",
            numero: "234"
        },
        {
            nom: "Depp",
            prenom: "Johnny",
            numero: "333"
        },
        {
            nom: "Carr",
            prenom: "Jimmy",
            numero: "666"
        }
    ];

    response.writeHead(200);
    const t = JSON.stringify(data);
    response.end(t);
});
const port = 4201;
app.listen(port);
console.log("Le serveur écoute sur le port " + port);
